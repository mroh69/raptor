package raptor.chess

import raptor.chess.util.GameUtils
import kotlin.experimental.and

// cuts the halfmove number and the movenumber out of the fen
fun toShortFen(fen:String): String {
    val fen_split = fen.split(' ')
    return "${fen_split[0]} ${fen_split[1]} ${fen_split[2]} ${fen_split[3]}"
}

fun isWhiteFen(fen:String) = fen.split(' ')[1] == "w"

fun ShortMoveToPair(move: String): Pair<Int, Int> {
    val from_square = GameUtils.getSquare(move)
    val to_square = GameUtils.getSquare(move.substring(2))
    return Pair(from_square, to_square)
}

// if the move is a smith castle (e1g1) is converts it to polyglot castle e1h1
fun convertMove(move: String, game:  Game): String {
    val (from_square, to_square) = ShortMoveToPair(move)
    val coloredFromPiece = GameUtils.getColoredPiece(from_square, game)

    if (GameUtils.getUncoloredPiece(coloredFromPiece) == GameUtils.KING) {
        when (move) {
            "e1g1" -> if (game.canWhiteCastleShort()) return "e1h1"
            "e1c1" -> if (game.canWhiteCastleLong()) return "e1a1"
            "e8g8" -> if (game.canBlackCastleShort()) return "e8h8"
            "e8c8" -> if (game.canBlackCastleLong()) return "e8a8"
            else -> {  // test for FRC Castle
                val coloredToPiece = GameUtils.getColoredPiece(to_square, game)
                if (GameUtils.getUncoloredPiece(coloredToPiece) == GameUtils.ROOK) {
                    if (coloredFromPiece == GameUtils.WK && coloredToPiece == GameUtils.WR) {
                        if (to_square > from_square && game.canWhiteCastleShort())
                            return "e1h1"
                        if (from_square > to_square && game.canWhiteCastleLong())
                            return "e1a1"
                    }
                    if (coloredFromPiece == GameUtils.BK && coloredToPiece == GameUtils.BR) {
                        if (to_square > from_square && game.canBlackCastleShort())
                            return "e8h8"
                        if (from_square > to_square && game.canBlackCastleLong())
                            return "e8a8"
                    }
                }
            }
        }
    }
    return move
}

fun sanToFigurine(san: String, style: String? = null): String {
    return if (style == null)
        san
    else {
        val s = StringBuilder()

        fun appendStyle(c: Char) {
            s.append("<span class=\"$style\">$c</span>")
        }

        for (c in san) {
            when (c) {
                'K' -> appendStyle('\u00C8')
                'Q' -> appendStyle('\u00CA')
                'R' -> appendStyle('\u00CB')
                'B' -> appendStyle('\u00CC')
                'N' -> appendStyle('\u00CD')
                'P' -> appendStyle('\u00CE')
                else -> s.append(c)
            }
        }
        s.toString()
    }
}

// converts g1f3 to Nf3
fun Game.moveStringToSan(move: String, isFRCMode:Boolean = true): String {
    if(move.isBlank())
        return ""
    val castlemove = moveStringToCastle(move, isFRCMode)
    if (castlemove != move)
        return castlemove
    val (from_square, to_square) = ShortMoveToPair(move)
    val theMove = Move(from_square, to_square, GameUtils.getUncoloredPiece(GameUtils.getColoredPiece(from_square, this)), this.colorToMove, GameUtils.getUncoloredPiece(GameUtils.getColoredPiece(to_square, this)))
    setSan(theMove)
    return theMove.san
}

fun Game.moveStringToCastle(move: String, isFRCMode:Boolean = true): String {
    if (((isFRCMode && "e1h1" == move) || (!isFRCMode && "e1g1" == move)) && canWhiteCastleShort()) {
        return "O-O"
    }
    if (((isFRCMode && "e1a1" == move) || (!isFRCMode && "e1c1" == move)) && canWhiteCastleLong()) {
        return "O-O-O"
    }
    if (((isFRCMode && "e8h8" == move) || (!isFRCMode && "e8g8" == move)) && canBlackCastleShort()) {
        return "O-O"
    }
    if (((isFRCMode && "e8a8" == move) || (!isFRCMode && "e8c8" == move)) && canBlackCastleLong()) {
        return "O-O-O"
    }
    return move
}

fun Game.makeConvertedMove(move:String, isFRCMode: Boolean = true): Move? {
    if(move.isBlank())
        return null

    val castleMove = moveStringToCastle(move, isFRCMode)
    if (castleMove != move) {
        return makeLanMove(castleMove)
    }
    else {
        val (from_square, to_square) = ShortMoveToPair(move)
        return makeMove(from_square, to_square)
    }
}

fun Game.pieceCount() =
    getPieceCount(GameConstants.WHITE, GameConstants.PAWN) +
    getPieceCount(GameConstants.WHITE, GameConstants.KNIGHT) +
    getPieceCount(GameConstants.WHITE, GameConstants.BISHOP) +
    getPieceCount(GameConstants.WHITE, GameConstants.ROOK) +
    getPieceCount(GameConstants.WHITE, GameConstants.QUEEN) +
    getPieceCount(GameConstants.WHITE, GameConstants.KING) +
    getPieceCount(GameConstants.BLACK, GameConstants.PAWN) +
    getPieceCount(GameConstants.BLACK, GameConstants.KNIGHT) +
    getPieceCount(GameConstants.BLACK, GameConstants.BISHOP) +
    getPieceCount(GameConstants.BLACK, GameConstants.ROOK) +
    getPieceCount(GameConstants.BLACK, GameConstants.QUEEN) +
    getPieceCount(GameConstants.BLACK, GameConstants.KING)

fun Move.toSmith() =
    if (isCastleShort) {
        if (isWhitesMove) "e1h1" else "e8h8"
    }
    else {
        if (isCastleLong) {
            if (isWhitesMove) "e1a1" else "e8a8"
        }
        else {
            GameUtils.getSan(getFrom()) + GameUtils.getSan(getTo()) +
            if (isPromotion)
                GameConstants.PIECE_TO_SAN[(piecePromotedTo and GameConstants.NOT_PROMOTED_MASK.toByte()).toInt()]
            else
                ""
        }
    }